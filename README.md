# Menpha
Welcome to Menpha (pronounced, 'men-fa'), a community-based item tracking platform. The app works by informing visitors who are considering purchasing an electronic device (say smartphone) to know if the item has been reported as stolen or missing. The result? You can make an informed decision whether to promote or curb the resell of stolen items.

Menpha depends on the unique code found on every smartphone or similar electronic device to set them apart from others.

The app has been designed to be straightforward, simple-to-use and easy as much as possible. Visit [Menpha](http://menpha.org) to get started. 

# Features
Menpha currently provides these features, with more to roll out gradually:
 - Simple signin/up with a secured third-party account, such as Google via OAUTH2
 - Adding unlimited items to your catalog, called 'My List'
 - Adding images to your items for easy recognition and comparison
 - Notifying a reporter of a seen electronic device in the wild (perhaps via an attempt of a resell)
 - Your own inbox/outbox messaging system for easy communications (no emails required, just usernames)
 
# Dependencies
Menpha depends on the following packages and frameworks:

**In the Python Department**
 - Python 2.7+
 - Django Framework 1.8+
 - django-ckeditor (used in the admin for easy editing of flatpages)
 - django-debug-toolbar (debugging stuffs)
 - django-imagekit (rendering smaller image size for templates)
 - django_compressor
 - django-allauth (Authentication like a boss, including social authentication)
 - django-postman (User-to-User messaging, yes, like a boss)
 - Selenium (for in-browser tests)

**FrontEnd**
 - MaterializeCSS Framework

# Testing
Menpha has been tested to some extent. To run tests, make the appropriate changes to `settings.py` and run `python manage.py tests` in the project root folder. 

You must have Chromedriver for Selenium installed and referenced in the appropriate directory.

# Contributing
Send in a Pull request or raise an issue and I'll be glad.

# License
See the License file