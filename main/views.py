# Views.py Handwritten by google.com/+Nkansahrexford
from django.db.models import Q
from django.shortcuts import render
from main.models import Item
from main.forms import ItemForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.core.urlresolvers import reverse_lazy


def home(request):
    """Your homepage with search bar"""
    return render(request, 'home.html')


@login_required
def profile(request):
    '''Displays profile name under your Account'''
    account = User.objects.get(username=request.user.get_username())
    return render(request, 'account.html', {'object': account})


def search(request):
    """ Displays results of the searched query"""
    query = request.GET.get('query', '')
    if query:
        qset = Q(slug__iexact=query, stolen__iexact='True')
        results = Item.objects.filter(qset).distinct()
    else:
        results = []

    return render(request, "search.html", {
        'object_list': results,
        'query': query,
    })


def direct_search(request, slug):
    """If someone searches for Item using directly like /search/<slug>"""
    query = Q(slug__iexact=slug, stolen__iexact='s')
    results = Item.objects.filter(query).distinct()
    return render(request, 'search.html', {
        'results': results,
        'user': request.user,
        'query': slug
    })


class CreateImei(CreateView):
    """ Adds an object to database by a user in context.
    In Menpha v2.0, using REST for adding items."""
    template_name = 'add.html'
    form_class = ItemForm
    model = Item

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CreateImei, self).form_valid(form)

    # requires login before allow saving
    @method_decorator(login_required())
    def dispatch(self, * args, ** kwargs):
        return super(CreateImei, self).dispatch(* args, ** kwargs)


class UpdateImei(UpdateView):
    """ Updates an object in context.

    If request.user is same as created_by value in model, dispatch
    function proceeds. Else PermissionDenied. Useful for preventing
    one user from editing item added by another user. """
    model = Item
    fields = ['device', 'slug', 'description', 'photo', 'stolen']
    template_name = 'item_form.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        pulled = Item.objects.get(slug=kwargs['slug'])
        if pulled.created_by == request.user:
            return super(UpdateImei, self).dispatch(request, *args, **kwargs)
        raise PermissionDenied


class DeleteImei(DeleteView):
    """ Deletes the the context object.
    If request.user is same as created_by value in model,
    dispatch function proceeds. Else PermissionDenied.

    Useful for preventing one user from deleting
    item added by another user. """
    model = Item
    context_object_name = 'to_delete'
    success_url = reverse_lazy('mylist')
    template_name = 'item_confirm_delete.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        grab = Item.objects.get(slug=kwargs['slug'])
        # Check if delete is authorized
        if grab.created_by == request.user:
            return super(DeleteImei, self).dispatch(request, *args, **kwargs)
        raise PermissionDenied


class ListImei(ListView):
    """Lists all the items added by a user."""
    model = Item
    template_name = 'mylist.html'

    def get_queryset(self):
        qset = Q(created_by=self.request.user)
        return Item.objects.filter(qset).distinct().order_by('-pub_date')

    @method_decorator(login_required())
    def dispatch(self, * args, ** kwargs):
        return super(ListImei, self).dispatch(* args, ** kwargs)


class ItemDetail(DetailView):
    model = Item
    template_name = 'detail.html'
    slug_field = 'slug'
    slug_url_kwarg = 'slug'


# class ListBlog(ListView):
#     model = Blog
#     template_name = 'blog.html'
#     context_object_name = 'list'


# class BlogDetail(DetailView):
#     slug_field = 'slug'
#     slug_url_kwarg = 'slug'
#     model = Blog
#     context_object_name = 'detail'
#     template_name = 'blog_detail.html'


# @login_required
# def notify_email(request, slug):
#     """ Send notification email to original owner of device who lost it """
#     grab = Item.objects.get(slug=slug)
#     if request.method == "POST":
#         form = NotifyEmailForm(request.POST)
#         if form.is_valid():
#             subject = form.cleaned_data['subject']
#             message = form.cleaned_data['message']
#             sender = request.user.email
#             recipients = [grab.created_by.email]
#             recipients.append(sender)

#             send_mail(subject, message, sender, recipients)
#             return HttpResponseRedirect('/notify/thanks')  # Redirect after POST
#     else:
#         form = NotifyEmailForm()  # An unbound form
#     return render(request, 'send-mail.html', {'form': form, 'object': slug, })


# Error Pages. Referred from Settings.py
def file_not_found_404(request):
    return render(request, '404.html')


def server_error(request):
    return render(request, '500.html')


def perm_denied(request):
    return render(request, '403.html')
