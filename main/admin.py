from django.contrib import admin
from main.models import Item, siteWide
from django.contrib.flatpages.models import FlatPage

# Note: we are renaming the original Admin and Form as we import them!
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld

from django import forms
from ckeditor.widgets import CKEditorWidget


class ItemAdmin(admin.ModelAdmin):
    list_display = ('device', 'slug', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['slug']
    date_hierarchy = 'pub_date'


# class BlogAdmin(admin.ModelAdmin):
#     prepopulated_fields = {'slug': ('title',)}


class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        # this is not automatically inherited from FlatpageFormOld
        model = FlatPage
        fields = ['id', 'title', 'content']


class FlatPageAdmin(FlatPageAdminOld):
    form = FlatpageForm


admin.site.register(Item, ItemAdmin)
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)
admin.site.register(siteWide)
# admin.site.register(Blog, BlogAdmin)
