from .models import siteWide


def staticPage(request):
    statics = siteWide.objects.all()

    return {
        'statics': statics,
    }
