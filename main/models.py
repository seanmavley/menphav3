from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill


class Item(models.Model):
    """ Main model for the Menpha app. """
    device = models.CharField(max_length=250,)
    slug = models.SlugField(max_length=30, unique=True,)
    description = models.TextField(max_length=500)
    stolen = models.BooleanField(default=False)
    photo = ProcessedImageField(blank=True, null=True, upload_to='devices/',
        processors=[ResizeToFill(920, 480)], format='JPEG',
        options={'quality': 80})
    update_on = models.DateTimeField(auto_now=True)
    pub_date = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User)

    def __unicode__(self):
        """ Returns device name """
        return self.device

    def get_absolute_url(self):
        """ For reverse url matching in views """
        return reverse('detail', kwargs={'slug': self.slug})


class siteWide(models.Model):
    meta_data = models.TextField(blank=True, default='meta things')
    plugins = models.TextField(blank=True, default='plugins')

    def __unicode__(self):
        return 'Meta Data and Plugin'