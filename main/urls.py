# Urls.py Handwritten by google.com/+Nkansahrexford
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from . import views
from django.conf.urls.static import static
from Menpha import settings

urlpatterns = patterns('',
    # Normal url for back and forth communications
    url(r'^$', views.home, name='home'),
    url(r'^search/$', views.search, name='search'),
    url(r'^search/(?P<slug>\d+)/$', views.direct_search, name='direct_search'),
    url(r'^add/$', views.CreateImei.as_view(), name='add'),
    url(r'^detail/(?P<slug>\d+)/$', views.ItemDetail.as_view(), name='detail'),
    url(r'^edit/(?P<slug>\d+)/$', views.UpdateImei.as_view(), name='edit'),
    # url(r'^notify/email/(?P<slug>\d+)/$', views.notify_email, name='notify_email'),
    # url(r'^notify/(?P<slug>\d+)/$', views.notify, name='notify'),
    url(r'^delete/(?P<slug>\d+)/$', views.DeleteImei.as_view(), name='delete'),
    url(r'^mylist/$', views.ListImei.as_view(), name='mylist'),
    # url(r'^blog/$', views.ListBlog.as_view(), name='blog'),
    # url(r'^blog/(?P<slug>[\-\w]+)/$', views.BlogDetail.as_view(), name='blog_detail'),
    url(r'^privacy/$', TemplateView.as_view(template_name='privacy.html'), name='privacy'),
    url(r'^notify/thanks/$', login_required(TemplateView.as_view(template_name='notify-thanks.html')),),
    url(r'^email/thanks/$', login_required(TemplateView.as_view(template_name='email-thanks.html')),),

    url(r'^accounts/profile/$', views.profile, name='myAccount'),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
