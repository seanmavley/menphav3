from django.test import TestCase
from django.core.urlresolvers import reverse, resolve
from django.contrib.auth.models import User
from selenium import webdriver


class WelcomeTests(TestCase):
    # fixtures = ['app_fixtures.json']

    def _create_user_and_login(self):
        user = User.objects.create_user(username='khophi')
        user.set_password('0011')
        user.save()
        self.client.login(username='khophi', password='0011')
        return user

    def test_login_view(self):
        response = self.client.get(reverse('account_login'))
        self.assertEqual(response.status_code, 200)

    def test_not_login_redirect_follow(self):
        response = self.client.get(reverse('mylist'))
        self.assertRedirects(response, reverse('account_login')+'?next=/mylist/')

    def test_logged_in_allow_access(self):
        self._create_user_and_login()
        response = self.client.get(reverse('mylist'))
        self.assertEqual(response.status_code, 200)


class TestHomePageSearch(TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome('/home/khophi/Downloads/chromedriver')

    def tearDown(self):
        self.browser.quit()

    def test_direct_search_not_found(self):
        # No response could be found
        response = self.client.get(reverse('search'), {'query': '008309283094820'})
        self.assertEqual(response.status_code, 200)
        self.assertIn('No results could be returned', response.content)

    def test_enter_search_into_field(self):
        print 'Ensure you have your test server also running at localhost:8000'
        self.browser.get('http://localhost:8000')
        search = self.browser.find_element_by_name('query')
        search.send_keys('0000000000000000')
        search.submit()


class TestItemsGeneral(TestCase):
    def _create_user_and_login(self):
        user = User.objects.create_user(username='khophi')
        user.set_password('0011')
        user.save()
        self.client.login(username='khophi', password='0011')
        return user

    post_data = {
        'device': 'Phone',
        'slug': '00000',
        'description': 'Phone',
        'stolen': True,
    }

    def test_add_view_denies_anonymous(self):
        response = self.client.get(reverse('add'))
        self.assertRedirects(response, reverse('account_login')+'?next=/add/')

        # This causes redirect, but I don't follow
        # but uses status_code to determine the page is redirected
        response = self.client.post(reverse('add'), self.post_data)
        self.assertRedirects(response, reverse('account_login')+'?next=/add/')
        self.assertEqual(response.status_code, 302)

    def test_add_item_by_logged_in(self):
        self._create_user_and_login()

        response = self.client.get(reverse('add'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'add.html')

    def test_add_item_with_login(self):
        ''' See if will post with login'''
        self._create_user_and_login()

        response = self.client.post(reverse('add'), self.post_data)
        print response
        self.assertEqual(response.status_code, 302)
        print resolve('/detail/00000/')

        request = self.client.get('/detail/00000/')

        # this always throws error of a f*#(*%) 'home' url I don't know
        # where it is.
        # response = self.client.get(reverse('detail', kwargs={'slug': 00000}))
        # self.assertEqual(response.status_code, 200)
